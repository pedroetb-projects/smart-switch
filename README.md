# smart-switch

Reverse proxy to access `smart-switch` device, exposed by Traefik

Check `smart-switch` repository to learn more about this device: <https://github.com/pedroetb/smart-switch>
